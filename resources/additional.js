var various_jobs_clicked = 0;
var valid_characters = 'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm- _1234567890+=\'';
var goal = 'This is a multiline text to be processed And Second Line is here';

function processClicks(clicks_number)
{
    if(clicks_number == 1)
    { $("#various-jobs-link").text('Stop It!'); }
    else if(clicks_number == 2)
    { $("#various-jobs-link").text('I Told You Stop It!'); }
    else if(clicks_number == 3)
    { $("#various-jobs-link").text('It is not funny!'); }
    else if(clicks_number == 4)
    { $("#various-jobs-link").text('STOP IT NOW!'); }
    else if(clicks_number == 5)
    { $("#various-jobs-link").text('OK, here You GO!'); }
}

function showScrumbleText(goal)
{
    var scrumbled = ''; 
    
    for(var i=0; i<goal.length; i++)
    {
        var random = Math.floor(Math.random() * (valid_characters.length - 0 + 1));
        scrumbled += valid_characters.substr(random, 1);
    }
    $('#various-jobs-1').text(scrumbled);
    return scrumbled;
}

function unscrumbleText(goal)
{
    var characters_to_roll = [];
    for(var i=0; i<goal.length; i++)
    {
        characters_to_roll.push(goal.substr(i, 1));
    }
    var unscrumbled = false;
    var iteration = 0;
    while (unscrumbled != true)
    {
        scrumbled = '';
        for(var i=0; i<characters_to_roll.length; i++)
        {
            if(characters_to_roll[i] != "!")
            {
                var random = Math.floor(Math.random() * (valid_characters.length - 0 + 1));
                scrumbled += valid_characters.substr(random, 1);
                if(valid_characters.substr(random, 1) == characters_to_roll[i])
                { characters_to_roll[i] = "!"; }
            }
            else
            {
                scrumbled += goal.substr(i, 1);
            }

        }
        iteration += 1;
        if(iteration % 10 == 1)
        {
        $('#various-jobs-1').text(scrumbled);

            iteration = 0;
        }
        console.log(scrumbled);
        unscrumbled = scrumbled == goal ? true : false;
    }
    $('#various-jobs-1').text(scrumbled);

}


function showVariousJobs()
{
    various_jobs_clicked += 1
    processClicks(various_jobs_clicked);
    if(various_jobs_clicked >= 5)
    {
        var scrumbled = showScrumbleText(goal);
        unscrumbleText(goal);
    }
    console.log(various_jobs_clicked);
    
}